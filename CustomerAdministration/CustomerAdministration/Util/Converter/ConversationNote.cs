﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerAdministration.Util.Converter
{
    public class ConversationNote : PropertyChangedBase
    {

        private DateTime _timeStamp;
        public DateTime TimeStamp
        {
            get { return _timeStamp; }
            set
            {
                _timeStamp = value;
                NotifyOfPropertyChange(null);
            }
        }

        private string _text = "";
        public string Text
        {
            get { return _text; }
            set
            {
                _text = value;
                NotifyOfPropertyChange(null);
            }
        }

        public ConversationNote(string text)
        {
            TimeStamp = DateTime.Now;
            Text = text;
        }

        public override string ToString()
        {
            return TimeStamp.ToLocalTime().ToString() + "|" + Text;
        }
    }

  
}
