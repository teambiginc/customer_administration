﻿using Caliburn.Micro;
using CsvHelper.TypeConversion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CustomerAdministration.Util.Converter
{
    public class NoteListTypeConverter : ITypeConverter
    {
        public bool CanConvertFrom(Type type)
        {
            return true;
        }

        public bool CanConvertTo(Type type)
        {
            return true;
        }

        public object ConvertFromString(TypeConverterOptions options, string text)
        {
            BindableCollection<ConversationNote> noteList = new BindableCollection<ConversationNote>();
            string pattern = @"(\d\d\.\d\d\.\d\d\d\d \d\d\:\d\d\:\d\d)([^\|]*)|";
            MatchCollection matches = Regex.Matches(text, pattern);
            foreach (Match match in matches)
            {
                if (match.Groups[1].Value.Trim() != "")
                {
                    ConversationNote note = new ConversationNote("");
                    note.TimeStamp = DateTime.Parse(match.Groups[1].Value.Trim());
                    note.Text = match.Groups[2].Value.Trim();
                    noteList.Add(note);
                }
            }
                
             
            
            return noteList;
        }

        public string ConvertToString(TypeConverterOptions options, object value)
        {
            string returnString = "";
            BindableCollection<ConversationNote> noteList = value as BindableCollection<ConversationNote>;
            if (noteList != null)
            {
                foreach (ConversationNote note in noteList)
                {
                    returnString += note.TimeStamp + " " + note.Text + "|";
                }
            }

            return returnString;
        }
    }
}
