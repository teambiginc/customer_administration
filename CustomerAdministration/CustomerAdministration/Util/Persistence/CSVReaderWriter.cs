﻿using Caliburn.Micro;
using CsvHelper;
using CustomerAdministration.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CustomerAdministration.Util.Persistence
{
    public class CSVReaderWriter
    {

        public static string ToCsv<T>(IEnumerable<T> objectlist)
        {

            string separator = "\t";
            Type t = typeof(T);
            FieldInfo[] fields = t.GetFields();

            string header = String.Join(separator, fields.Select(f => f.Name).ToArray());

            StringBuilder csvdata = new StringBuilder();
            csvdata.AppendLine(header);

            foreach (var o in objectlist)
                csvdata.AppendLine(ToCsvFields(separator, fields, o));

            return csvdata.ToString();
        }

        public static string ToCsvFields(string separator, FieldInfo[] fields, object o)
        {
            StringBuilder linie = new StringBuilder();

            foreach (var f in fields)
            {
                if (linie.Length > 0)
                    linie.Append(separator);

                var x = f.GetValue(o);

                if (x != null)
                    linie.Append(x.ToString());
            }

            return linie.ToString();
        }

        public static void saveCustomers(BindableCollection<Customer> list)
        {
            string path = @"KundenDatenbank\";
            if (!Directory.Exists(@System.AppDomain.CurrentDomain.BaseDirectory + @path))
            {

                // Try to create the directory.
                DirectoryInfo di = Directory.CreateDirectory(@System.AppDomain.CurrentDomain.BaseDirectory + @path);
                Console.WriteLine("The directory was created successfully at {0}.",
                    Directory.GetCreationTime(@System.AppDomain.CurrentDomain.BaseDirectory + @path));
            }
            using (TextWriter tw = File.CreateText(@System.AppDomain.CurrentDomain.BaseDirectory + @"KundenDatenbank\KundenDatenbank.csv"))
            {
                CsvWriter csv = new CsvWriter(tw);
                csv.Configuration.RegisterClassMap<CustomerClassMap>();
                csv.Configuration.Delimiter = "\t";
                csv.WriteRecords(list);
            }

        }

        public static BindableCollection<Customer> loadCustomers()
        {
            BindableCollection<Customer>  customers = new BindableCollection<Customer>();
         try
            {

                using (TextReader tr = File.OpenText(@System.AppDomain.CurrentDomain.BaseDirectory + @"KundenDatenbank\KundenDatenbank.csv"))
                {

                    CsvReader csv = new CsvReader(tr);
                    csv.Configuration.RegisterClassMap<CustomerClassMap>();
                    csv.Configuration.Delimiter = "\t";
                    csv.Configuration.IgnoreReadingExceptions = true;
                    csv.Configuration.HasHeaderRecord = true;
                    csv.Configuration.SkipEmptyRecords = false;
                    var records = csv.GetRecords<Customer>().ToList();

                    foreach(var record in records)
                    {
                        customers.Add(record);
                    }
    
                }
            }
            catch (Exception)
            {

               
            }




                return customers;
        }
    }
}
