﻿using CustomerAdministration.Util;
using CustomerAdministration.Util.Converter;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerAdministration.Enums
{
    [TypeConverter(typeof(EnumDescriptionTypeConverter))]
    public enum CONTACTSTATUS
    {
        [Description("Neu")]
        NEW,
        [Description("Nicht erreicht")]
        NOTREACHED,
        [Description("Kein Interesse")]
        NOINTEREST,
        [Description("Kontakt + Mail raus")]
        CONTACTANDMAILSENT,
        [Description("WVL")]
        WVL,
        [Description("Angebot raus")]
        OFFERSENT,
        [Description("Vertrag raus")]
        CONTRACTSENT,
        [Description("Rahmenvertrag raus")]
        FRAMEWORKCONTRACTSENT,
        [Description("Beauftragt")]
        COMISSIONED,
        [Description("Besetzt")]
        BUSY,
        [Description("Rechnung raus")]
        INVOICESENT,
        [Description("Rechnung beglichen")]
        INVOICEMET,
        [Description("Abgeschlossen")]
        DONE,
    }
}
