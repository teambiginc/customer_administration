﻿using CustomerAdministration.Util;
using CustomerAdministration.Util.Converter;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerAdministration.Enums
{
    [TypeConverter(typeof(EnumDescriptionTypeConverter))]
    public enum CUSTOMERSTATUS
    {
        [Description("Neu")]
        NEW,
        [Description("Bestand")]
        STOCK
    }
}
