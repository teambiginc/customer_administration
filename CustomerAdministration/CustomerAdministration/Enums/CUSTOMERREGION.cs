﻿using CustomerAdministration.Util;
using CustomerAdministration.Util.Converter;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerAdministration.Enums
{
    [TypeConverter(typeof(EnumDescriptionTypeConverter))]
    public enum CUSTOMERREGION
    {
        [Description("Türkei")]
        TURKEY,
        [Description("Iran")]
        IRAN,
        [Description("Aserbaidschan")]
        ASERBAIDSCHAN,
        [Description("Deutschland")]
        GERMANY,
        [Description("Sonstige")]
        OTHERS,
    }
}
