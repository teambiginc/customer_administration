﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerAdministration.Models
{
    public class Address
    {
        public string Zip { get; set; }
        public String City { get; set; }
        public String Street { get; set; }
        public String StreetNr { get; set; }


        public Address()
        {
            Zip = "";
            City = "";
            Street = "";
            StreetNr = "";
        }

        public override string ToString()
        {
            string returnString ="";

            if(Street != "")
            {
                returnString += Street;
                if (StreetNr != "")
                {
                    returnString += " " + StreetNr;
                }
               
            }
            if (Zip != "")
            {
                if(Street != "")
                {
                    returnString += ", ";
                }
                returnString += Zip;
            }
            if (City != "")
            {
                returnString += ", " + City;
            }



            return returnString;
        }
    }
}
