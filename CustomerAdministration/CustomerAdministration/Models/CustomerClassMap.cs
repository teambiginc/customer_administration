﻿using CsvHelper.Configuration;
using CustomerAdministration.Enums;
using CustomerAdministration.Util.Converter;

namespace CustomerAdministration.Models
{
    public sealed class CustomerClassMap : CsvClassMap<Customer>
    {
        public CustomerClassMap()
        {
            Map(m => m.ID);
            Map(m => m.CompanyName);
            Map(m => m.CustomerStatus).TypeConverter<EnumCSVTypeConverter<CUSTOMERSTATUS>>();
            Map(m => m.AP);
            Map(m => m.TelNr);
            Map(m => m.Mail);
            Map(m => m.Vacancy);
            Map(m => m.Region).TypeConverter<EnumCSVTypeConverter<CUSTOMERREGION>>();
            Map(m => m.ContactStatus).TypeConverter<EnumCSVTypeConverter<CONTACTSTATUS>>();
            Map(m => m.ActualNote);
            References<AddressClassMap>(m => m.InvoiceAddress);
            Map(m => m.Sales);
            Map(m => m.NoteList).TypeConverter<NoteListTypeConverter>();
        }
    }
}