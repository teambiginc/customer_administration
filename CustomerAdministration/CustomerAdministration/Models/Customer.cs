﻿using Caliburn.Micro;
using CustomerAdministration.Enums;
using CustomerAdministration.Util.Converter;
using System;
using System.Linq;

namespace CustomerAdministration.Models
{
    public class Customer : PropertyChangedBase, ICloneable
    {
        #region members

        public BindableCollection<ConversationNote> NoteList { get; set; }

        private long _iD;
        public long ID
        {
            get { return _iD; }
            set
            {
                _iD = value;
                NotifyOfPropertyChange(null);
            }
        }

        private string _companyName;
        public string CompanyName
    {
            get { return _companyName; }
            set
            {
            _companyName = value;
                NotifyOfPropertyChange(null);
            }
        }

        private CUSTOMERSTATUS _customerStatus;
        public CUSTOMERSTATUS CustomerStatus
    {
            get { return _customerStatus; }
            set
            {
            _customerStatus = value;
                NotifyOfPropertyChange(null);
            }
        }

        private string _ap;
        public string AP
        {
            get { return _ap; }
            set
            {
                _ap = value;
                NotifyOfPropertyChange(null);
            }
        }

        private string _telNr;
        public string TelNr
    {
            get { return _telNr; }
            set
            {
                _telNr = value;
                NotifyOfPropertyChange(null);
            }
        }

        private string _mail;
        public string Mail
        {
            get { return _mail; }
            set
            {
                _mail = value;
                NotifyOfPropertyChange(null);
            }
        }

        private string _vacancy;
        public string Vacancy
        {
            get { return _vacancy; }
            set
            {
                _vacancy = value;
                NotifyOfPropertyChange(null);
            }
        }

        private CUSTOMERREGION _region;
        public CUSTOMERREGION Region
        {
            get { return _region; }
            set
            {
                _region = value;
                NotifyOfPropertyChange(null);
            }
        }

        private CONTACTSTATUS _contactStatus;
        public CONTACTSTATUS ContactStatus
        {
            get { return _contactStatus; }
            set
            {
                _contactStatus = value;
                NotifyOfPropertyChange(null);
            }
        }

       
        public string ActualNote
        {
            get {
                if(NoteList.Count>0)
                {
                    return NoteList.ElementAt(0).Text;
                    
                }
                else
                {
                    return "";
                }
                ; }
           
        }

        private Address _invoiceAddress;
        public Address InvoiceAddress
        {
            get { return _invoiceAddress; }
            set
            {
                _invoiceAddress = value;
                NotifyOfPropertyChange(null);
            }
        }

        private string _sales;
        public string Sales
        {
            get { return _sales; }
            set
            {
                _sales = value;
                NotifyOfPropertyChange(null);
            }
        }
        #endregion
        #region functions
        private void NoteListChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            NotifyOfPropertyChange(() => ActualNote);
        }
     
     
        public string NoteListValues
        {
            get
            {
                return string.Join("^", this.NoteList.Select(c => c));
     
            }
        }

        public void setValuesFrom(Customer customer)
        {
            CompanyName = customer.CompanyName;
            CustomerStatus = customer.CustomerStatus;
            InvoiceAddress = customer.InvoiceAddress;
            Region = customer.Region;
            ContactStatus = customer.ContactStatus;
            TelNr = customer.TelNr;
            AP = customer.AP;
            Mail = customer.Mail;
            Vacancy = customer.Vacancy;
            NoteList = customer.NoteList;
            Sales = customer.Sales;
            ID = customer.ID;
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
        #endregion
        #region constructor
        public Customer(long id)
        {
           
            CompanyName = "";
            CustomerStatus = CUSTOMERSTATUS.NEW;
            InvoiceAddress = new Address();
            Region = CUSTOMERREGION.GERMANY;
            ContactStatus = CONTACTSTATUS.NEW;
            TelNr = "";
            AP = "";
            Mail = "";
            Vacancy = "";
            NoteList = new BindableCollection<ConversationNote>();
            NoteList.CollectionChanged += NoteListChanged;
            Sales = "";
            ID = id;
        }

        public Customer()
        {

            CompanyName = "";
            CustomerStatus = CUSTOMERSTATUS.NEW;
            InvoiceAddress = new Address();
            Region = CUSTOMERREGION.GERMANY;
            ContactStatus = CONTACTSTATUS.NEW;
            TelNr = "";
            AP = "";
            Mail = "";
            Vacancy = "";
            NoteList = new BindableCollection<ConversationNote>();
            NoteList.CollectionChanged += NoteListChanged;
            Sales = "";
            ID = 0;
        }
        #endregion
    }
}
