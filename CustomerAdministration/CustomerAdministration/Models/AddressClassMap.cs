﻿using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomerAdministration.Models
{
    /**/
    public class AddressClassMap : CsvClassMap<Address>
    {
        public AddressClassMap()
        {
            Map(m => m.Street);
            Map(m => m.StreetNr);
            Map(m => m.City);
            Map(m => m.Zip);
        } }
}
