﻿using Caliburn.Micro;
using CustomerAdministration.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace CustomerAdministration
{
    public enum CustomerAdministrationStates
    {
        START,NEW, EDIT, ENDEDIT, DELETE, SELECTED, NOSELECTION
    }

    public enum CustomerAdministrationTriggers
    {
        STARTINGDONE, SELECT, NEW, EDIT, DELETE, CANCEL, SAVE
    }

    public static class MyStateMachine
    {
        private static CustomerAdministrationStates _state;

        public static void setState(CustomerViewModel customerViewModel, CustomerAdministrationStates state)
        {
            switch(state)
            {
                case CustomerAdministrationStates.START:
                    customerViewModel.CustomerListEnabled = true;
                    customerViewModel.CustomerDetailEnabled = false;
                    customerViewModel.CustomerNotesEnabled = false;
                    break;
                case CustomerAdministrationStates.NEW:
                    customerViewModel.CustomerListEnabled = false;
                    customerViewModel.CustomerDetailEnabled = true;
                    customerViewModel.CustomerNotesEnabled = false;
                    break;
                case CustomerAdministrationStates.EDIT:
                    customerViewModel.CustomerListEnabled = false;
                    customerViewModel.CustomerDetailEnabled = true;
                    customerViewModel.CustomerNotesEnabled = true;
                    break;
                case CustomerAdministrationStates.SELECTED:
                    customerViewModel.CustomerListEnabled = true;
                    customerViewModel.CustomerDetailEnabled = false;
                    customerViewModel.CustomerNotesEnabled = true;
                    break;
                case CustomerAdministrationStates.NOSELECTION:
                    customerViewModel.CustomerListEnabled = true;
                    customerViewModel.CustomerDetailEnabled = false;
                    customerViewModel.CustomerNotesEnabled = false;
                    break;
            }
           
                
            
          
            _state = state;
        }

        public static CustomerAdministrationStates getState()
        {
            return _state;
        }
    }
 
    public class StateMachine : Stateless.StateMachine<CustomerAdministrationStates, CustomerAdministrationTriggers>, INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;

        public StateMachine(System.Action searchAction) : base(CustomerAdministrationStates.START)
        {
            Configure(CustomerAdministrationStates.START)
              //.Permit(CustomerAdministrationTriggers.STARTINGDONE, CustomerAdministrationStates.SELECTED)
              .Permit(CustomerAdministrationTriggers.STARTINGDONE, CustomerAdministrationStates.NOSELECTION);

            Configure(CustomerAdministrationStates.SELECTED)
              .Permit(CustomerAdministrationTriggers.NEW, CustomerAdministrationStates.NEW)
              .Permit(CustomerAdministrationTriggers.EDIT, CustomerAdministrationStates.EDIT)
              .Permit(CustomerAdministrationTriggers.DELETE, CustomerAdministrationStates.SELECTED)
              //.Permit(CustomerAdministrationTriggers.DELETE, CustomerAdministrationStates.NOSELECTION)
              .Ignore(CustomerAdministrationTriggers.CANCEL)
              .Ignore(CustomerAdministrationTriggers.SAVE);

            //Configure(States.SearchComplete)
            //  .SubstateOf(States.Start)
            //  .Permit(Triggers.Select, States.Selected)
            //  .Permit(Triggers.DeSelect, States.NoSelection);

            //Configure(States.Selected)
            //  .SubstateOf(States.SearchComplete)
            //  .Permit(Triggers.DeSelect, States.NoSelection)
            //  .Permit(Triggers.Edit, States.Editing)
            //  .Ignore(Triggers.Select);

            //Configure(States.NoSelection)
            //  .SubstateOf(States.SearchComplete)
            //  .Permit(Triggers.Select, States.Selected)
            //  .Ignore(Triggers.DeSelect);

            //Configure(States.Editing)
            //  .Permit(Triggers.EndEdit, States.Selected);

            OnTransitioned
              (
                (t) =>
                {
                    OnPropertyChanged("State");
                    CommandManager.InvalidateRequerySuggested();
                }
              );

            //used to debug commands and UI components
            OnTransitioned
              (
                (t) => Debug.WriteLine
                  (
                    "State Machine transitioned from {0} -> {1} [{2}]",
                    t.Source, t.Destination, t.Trigger
                  )
              );
        }
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}