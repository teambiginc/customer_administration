﻿using Caliburn.Micro;
using CustomerAdministration.Enums;
using CustomerAdministration.Models;
using CustomerAdministration.Util;
using CustomerAdministration.Util.Converter;
using CustomerAdministration.Util.Persistence;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Data;

namespace CustomerAdministration.ViewModels
{
    public class CustomerViewModel : Screen
    {


        private string _searchText;

        public string SearchText
        {
            get
            {
                return _searchText;
            }
            set
            {
                _searchText = value;
                NotifyOfPropertyChange(() => SearchText);
                
                if (String.IsNullOrEmpty(value))
                    CustomersView.Filter = null;
                else
                    CustomersView.Filter = new Predicate<object>(o => ((Customer)o).CompanyName.Contains(value, StringComparison.OrdinalIgnoreCase));
                
                 NotifyAllButtons();
            }
        }
        


        private bool _customerDetailEnabled;

        public bool CustomerDetailEnabled
        {
            get
            {
                return _customerDetailEnabled;
            }
            set
            {
                _customerDetailEnabled = value;
                NotifyOfPropertyChange(() => CustomerDetailEnabled);
            }
        }

        private bool _customerListEnabled;

        public bool CustomerListEnabled
        {
            get
            {
                return _customerListEnabled;
            }
            set
            {
                _customerListEnabled = value;
                NotifyOfPropertyChange(() => CustomerListEnabled);
            }
        }

        private bool _customerNotesEnabled;

        public bool CustomerNotesEnabled
        {
            get
            {
                return _customerNotesEnabled;
            }
            set
            {
                _customerNotesEnabled = value;
                NotifyOfPropertyChange(() => CustomerNotesEnabled);
            }
        }

        private Customer _selectedCustomer;

        private Customer _selectedCustomerBeforeCreate;

        private Customer _customerInCreation;

        public string CustomerNoteInCreation
        {
            get
            {
                return _customerNoteInCreation;
            }
            set
            {
                _customerNoteInCreation = value;
                NotifyOfPropertyChange(() => CustomerNoteInCreation);
            }
        }

        private string _customerNoteInCreation;

        public BindableCollection<Customer> Customers { get; private set; }

        public ListCollectionView CustomersView { get; private set; }

        public Customer SelectedCustomer
        {
            get
            {
                return _selectedCustomer;
            }
            set
            {
                _selectedCustomer = value;
                NotifyOfPropertyChange(() => SelectedCustomer);
                NotifyOfPropertyChange(() => CustomerStatusTypeValues);
            }
        }

        private ConversationNote _selectedConversationNote;
        public ConversationNote SelectedConversationNote
        {
            get
            {
                return _selectedConversationNote;
            }
            set
            {
                _selectedConversationNote = value;
                NotifyOfPropertyChange(() => SelectedConversationNote);
            }
        }

        public IEnumerable<CUSTOMERSTATUS> CustomerStatusTypeValues
        {
            get
            {
                return Enum.GetValues(typeof(CUSTOMERSTATUS))
                    .Cast<CUSTOMERSTATUS>();
            }
        }

        public IEnumerable<CUSTOMERREGION> CustomerRegionTypeValues
        {
            get
            {
                return Enum.GetValues(typeof(CUSTOMERREGION))
                    .Cast<CUSTOMERREGION>();
            }
        }

        public IEnumerable<CONTACTSTATUS> ContactStatusTypeValues
        {

            get
            {
                return Enum.GetValues(typeof(CONTACTSTATUS))
                    .Cast<CONTACTSTATUS>();
            }
        }



        public CustomerViewModel()
        {
      
            Customers = CSVReaderWriter.loadCustomers();
            CustomersView = new ListCollectionView(Customers);
            
            if (Customers.Count > 0)
            {
                SelectedCustomer = Customers.ElementAt(0);
                MyStateMachine.setState(this, CustomerAdministrationStates.SELECTED);
            }
            else
            {
                MyStateMachine.setState(this, CustomerAdministrationStates.NOSELECTION);
            }
                
        }

        public bool CanEditCustomer
        {
            get
            {

                return MyStateMachine.getState() == CustomerAdministrationStates.SELECTED;
            }
        }

        public void EditCustomer()
        {
            MyStateMachine.setState(this,CustomerAdministrationStates.EDIT);
            _selectedCustomerBeforeCreate = SelectedCustomer;
            _customerInCreation = (Customer)SelectedCustomer.Clone();
            SelectedCustomer = _customerInCreation;

            NotifyAllButtons();
        }

        public bool CanUpdateSearchFilter
        {
            get
            {

                return true;
            }
        }

        public void UpdateSearchFilter()
        {  
            NotifyAllButtons();
        }

        public bool CanRemoveSearch
        {
            get
            {

                return true;
            }
        }

        public void RemoveSearch()
        {
            SearchText = "";

            NotifyAllButtons();
        }

       

        public void SendMail(string text)
        {
            Process.Start("mailto:"+text);
           
        }

        public bool CanCreateCustomer
        {
            get
            {

                return MyStateMachine.getState() == CustomerAdministrationStates.SELECTED | MyStateMachine.getState() == CustomerAdministrationStates.NOSELECTION;
            }
        }

        public void CreateCustomer()
        {
            MyStateMachine.setState(this, CustomerAdministrationStates.NEW);
            _selectedCustomerBeforeCreate =  SelectedCustomer;
            _customerInCreation = new Customer(Customers.Count+1);
            SelectedCustomer = _customerInCreation;
       
            NotifyAllButtons();

        }

        public bool CanDeleteCustomer
        {
            get
            {
                return MyStateMachine.getState() == CustomerAdministrationStates.SELECTED;
            }
        }

        public void DeleteCustomer()
        {
            MyStateMachine.setState(this, CustomerAdministrationStates.DELETE);
            if(SelectedCustomer != null)
            {
                Customers.Remove(SelectedCustomer);
            }
            NotifyAllButtons();
            if(Customers.Count>0)
            {
                SelectedCustomer = Customers.ElementAt(0);
                MyStateMachine.setState(this, CustomerAdministrationStates.SELECTED);
            }
            else

            {
                MyStateMachine.setState(this, CustomerAdministrationStates.NOSELECTION);
            }

            CSVReaderWriter.saveCustomers(Customers);
            NotifyAllButtons();
        }
        

             public bool CanSaveCustomerNote
        {
            get
            {
                return true;
            }
        }

        public void SaveCustomerNote()
        {
            SelectedCustomer.NoteList.Insert(0,new ConversationNote(CustomerNoteInCreation));
            CustomerNoteInCreation = "";
            if(SelectedConversationNote == null)
            {
                SelectedConversationNote = SelectedCustomer.NoteList.ElementAt(0);
            }
            CSVReaderWriter.saveCustomers(Customers);
            NotifyAllButtons();
        }

        public bool CanDeleteCustomerNote
        {
            get
            {
                return true;
            }
        }

        public void DeleteCustomerNote()
        {
            SelectedCustomer.NoteList.Remove(SelectedConversationNote);       
            NotifyAllButtons();
        }
        

        public bool CanSaveCustomer
        {
            get
            {
                return MyStateMachine.getState() == CustomerAdministrationStates.EDIT | MyStateMachine.getState() == CustomerAdministrationStates.NEW;
            }
        }

        public void SaveCustomer()
        {
            if (MyStateMachine.getState() == CustomerAdministrationStates.NEW)
            {
                Customers.Add(_customerInCreation);
                

            }

            else if (MyStateMachine.getState() == CustomerAdministrationStates.EDIT)
            {
                Customer customer = Customers.Where(c => c.ID == _customerInCreation.ID).FirstOrDefault();       
                customer.setValuesFrom(_customerInCreation);
               
            }
            
            _customerInCreation = null;

            CSVReaderWriter.saveCustomers(Customers);
            MyStateMachine.setState(this, CustomerAdministrationStates.SELECTED);
            NotifyAllButtons();
        }

        public bool CanCancelCustomer
        {
            get
            {
                return MyStateMachine.getState() == CustomerAdministrationStates.EDIT | MyStateMachine.getState() == CustomerAdministrationStates.NEW;
            }
        }

        public void CancelCustomer()
        {
            if (MyStateMachine.getState() == CustomerAdministrationStates.NEW)
            {
                if (_selectedCustomerBeforeCreate != null)
                {
                    MyStateMachine.setState(this, CustomerAdministrationStates.SELECTED);
                }
                else
                {
                    MyStateMachine.setState(this, CustomerAdministrationStates.NOSELECTION);
                }
                
            }
            if (MyStateMachine.getState() == CustomerAdministrationStates.EDIT)
            {
                MyStateMachine.setState(this, CustomerAdministrationStates.SELECTED);
            }
            SelectedCustomer = _selectedCustomerBeforeCreate;


            NotifyAllButtons();
            
        }

        public void NotifyAllButtons()
        {
            NotifyOfPropertyChange(() => CanEditCustomer);
            NotifyOfPropertyChange(() => CanCreateCustomer);
            NotifyOfPropertyChange(() => CanDeleteCustomer);
            NotifyOfPropertyChange(() => CanSaveCustomer);
            NotifyOfPropertyChange(() => CanCancelCustomer);
        }
    }
}
