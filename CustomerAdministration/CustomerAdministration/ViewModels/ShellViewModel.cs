
using Caliburn.Micro;


namespace CustomerAdministration.ViewModels
{
 
    public class ShellViewModel : Conductor<Screen>.Collection.AllActive, IShell {

        public CustomerViewModel CustomersViewModel { get; set;}

        public ShellViewModel(CustomerViewModel customersViewModel)
        {
            CustomersViewModel = customersViewModel;
            base.DisplayName = "Kunden Verwaltung";
        }

     
    }
}